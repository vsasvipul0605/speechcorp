import Head from "next/head";
import { Swipeable } from "react-touch" ;

let i = 0;
const testimonialArray = [
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 1",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 2",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 3",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 4",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 5",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
  {
    src: "https://dummyimage.com/302x302",
    name: "Mr./Mrs User No. 6",
    text:
      "Edison bulb retro cloud bread echo park, helvetica stumptown taiyaki taxidermy 90's cronut +1 kinfolk. Single-origin coffee ennui shaman taiyaki vape DIY tote bag drinking vinegar cronut adaptogen squid fanny pack vaporware.",
  },
];
export default function Home() {
  const FeaturesState = React.useState(1);
  const selectedIndex = FeaturesState[0];
  const setSelectedIndex = FeaturesState[1];

  const checkNext = () => {
    const labels = document.querySelectorAll("#slider label");
    const nextIndex =
      FeaturesState[0] === labels.length - 1 ? 0 : FeaturesState[0] + 1;
    FeaturesState[1](nextIndex);
  };
  const checkPrev = () => {
    const labels = document.querySelectorAll("#slider label");
    const prevIndex =
      FeaturesState[0] === labels.length - 3 ? 2 : FeaturesState[0] - 1;
    FeaturesState[1](prevIndex);
  };
  const check = (index) => setSelectedIndex(index);

  // testimonial slider
  const TestimonialState = React.useState(1);
  const selectedTestimonialIndex = TestimonialState[0];
  const setSelectedTestimonialIndex = TestimonialState[1];

  const testimonialCheckNext = () => {
    const labels = document.querySelectorAll("#testimonialSlider label");
    const nextIndex =
      selectedTestimonialIndex === labels.length - 1
        ? 0
        : selectedTestimonialIndex + 1;
    setSelectedTestimonialIndex(nextIndex);

    if (i < testimonialArray.length - 1) {
      i++;
    } else {
      i = 0;
    }
  };

  const testimonialCheckPrev = () => {
    const labels = document.querySelectorAll("#testimonialSlider label");
    const prevIndex =
      selectedTestimonialIndex === labels.length - 3
        ? 2
        : selectedTestimonialIndex - 1;
    setSelectedTestimonialIndex(prevIndex);

    if (i > 0) {
      i--;
    } else {
      i = testimonialArray.length - 1;
    }
  };

  const testimonialCheck = (index) => setSelectedTestimonialIndex(index);
  
  const playVideo = (item) => {
    $("#video").trigger("play");
    $("#audio").trigger("play");
  };
  const pauseVideo = () => {
    $("#video").trigger("pause");
    $("#audio").trigger("pause");
  };
  const checkIcon = () => {
    $(".play, .pause").toggle();
  };

  return (
    <div className="App">
      <Head>
        <title>SpeechCorp</title>
        <link rel="icon" href="/favicon.jpg" />
        <link rel="stylesheet" href="css/style.css"></link>
        <link
          href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css"
          rel="stylesheet"
        />
        <script src="js/script.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
      </Head>

      <main>
      <section className="text-gray-700 body-font">
        <header className="shadow lg:px-16 px-6 bg-white flex flex-wrap items-center lg:py-0 py-2">
          <div className="flex-1 flex justify-between items-center">
            <a href="#">
            <img alt="logo" className="w-40 mb-2" src="logo.png"></img>
            </a>
          </div>

          <label for="menu-toggle" className="pointer-cursor lg:hidden block">
            <svg
              className="fill-current text-gray-900"
              xmlns="http://www.w3.org/2000/svg"
              width="20"
              height="20"
              viewBox="0 0 20 20"
            >
              <title>MENU</title>
              <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"></path>
            </svg>
          </label>
          <input className="hidden" type="checkbox" id="menu-toggle" />

          <div
            className="hidden lg:flex lg:items-center lg:w-auto w-full"
            id="menu"
          >
            <nav>
              <ul className="lg:flex items-center justify-between text-base text-gray-700 pt-4 lg:pt-0">
                <li>
                  <a
                    className="lg:p-4 lg:mr-32 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                    href="#"
                  >
                    About Us
                  </a>
                </li>
                <li>
                  <a
                    className="lg:p-4 lg:mr-32 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                    href="#"
                  >
                    Pricing
                  </a>
                </li>
                <li>
                  <a
                    className="lg:p-4 lg:mr-32 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400"
                    href="#"
                  >
                    Contact
                  </a>
                </li>
                <li>
                  <a
                    className="font-bold lg:p-4 py-3 px-0 block border-b-2 border-transparent hover:border-indigo-400 lg:mb-0 mb-2"
                    href="#"
                  >
                    Sign In
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </header>
        </section>
        {/* hero section */}
        <section className="text-gray-700 body-font">
          <div className="container mx-auto flex px-5 sm:py-24 mt-16 sm:mt-0 md:flex-row flex-col items-center">
            <div className="lg:flex-grow lg:w-1/2 md:w-1/2 flex flex-col md:items-start md:text-left mb-16 md:mb-0 items-center text-center">
              <h1 className="title-font sm:text-4xl text-3xl mb-4 font-bold text-gray-900">
                <span>Taking Notes Made Easy</span>
                {/* 
                <span>Background Noise Hassle Free</span> */}
              </h1>
              <br className="hidden lg:inline-block" />
              <h1 className="title-font text-xl font-medium text-gray-500 mb-3">
                Automated speech to text technology delivering{" "}
                <br className="hidden lg:inline-block" /> high quality results
                in less time.
              </h1>
            </div>
            <div className="lg:w-1/2 md:w-1/2 items-start">
              <div
                id="audio_home"
                className="flex flex-wrap items-center justify-center"
              >
                <div className="flex audio-bar-sm">
                  <img
                    onClick={() => {
                      playVideo();
                      checkIcon();
                    }}
                    alt="logo"
                    className="pl-pa-btn play w-30 h-10 m-auto"
                    src="play.svg"
                  ></img>
                  <img
                    onClick={() => {
                      pauseVideo();
                      checkIcon();
                    }}
                    alt="logo"
                    className="pl-pa-btn hidden pause w-30 h-10 m-auto"
                    src="pause.svg"
                  ></img>
                  <video loop id="video">
                    <source src="./audio.mp4" type="video/mp4" />
                  </video>
                  <audio id="audio" className="hidden" controls>
                    <source src="song.mp3" />
                  </audio>
                  <img
                    alt="logo"
                    className="vol-btn w-30 h-10 m-auto"
                    src="speaker.svg"
                  ></img>
                </div>
                <div
                  id="audio_footer"
                  className="border-solid border-2 border-gray-500 bg-gray-100 p-4 rounded-md text-black-200 font-medium"
                >
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut ero labore et dolore
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* help button */}

        <section className="text-gray-700 body-font">
          <div className="container px-5 py-0 mx-auto">
            <div className="lg:w-full justify-center flex-col sm:flex-row sm:items-center col-reverse pr-0 mx-auto">
              <button className="flex-shrink-0 w-64 h-12 text-white bg-blue-500 border-0 py-2 px-8 focus:outline-none hover:bg-blue-600 rounded text-lg mt-10 sm:mt-0">
                Contact Now
              </button>
            </div>
          </div>
        </section>

        <br></br>
        <br></br>
        <br></br>
        <br></br>
        {/* FEATURE */}

        <section className="text-gray-700 body-font">
          <div className="text-center mb-5">
            <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900 mb-4">
              Features
            </h1>
            <div className="w-16 h-1 rounded-full bg-blue-500 inline-flex"></div>
          </div>

          <br></br>
          <br></br>
          <div className="flex sm:flex-none flex-wrap mt-10 md:justify-center">
            <div className="hidden lg:w-1/4 py-48 mr-0 md:mb-0 mb-6 md:flex md:flex-col text-center justify-center items-end">
              <div className="w-20 h-20 hidden md:inline-flex items-center justify-center rounded-full lg:bg-blue-100 mb-5 flex-shrink-0">
                <button onClick={checkPrev}>
                  <img
                    alt="logo"
                    className="prev w-30 h-10 mb-1"
                    src="back.svg"
                  ></img>
                </button>
              </div>
            </div>
            <div className="lg:w-2/4 md:mb-0 mb-6 flex flex-col text-center items-center justify-center">
              <Swipeable onSwipeLeft={checkNext} onSwipeRight={checkPrev}>
              <section
                id="slider"
                className="w-16 h-20 inline-flex items-center justify-center mb-5 flex-shrink-0"
              >
                <input
                  type="radio"
                  name="slider"
                  id="s1"
                  checked={selectedIndex === 0}
                  onClick={() => check(0)}
                />
                <input
                  type="radio"
                  name="slider"
                  id="s2"
                  checked={selectedIndex === 1}
                  onClick={() => check(1)}
                />
                <input
                  type="radio"
                  name="slider"
                  id="s3"
                  checked={selectedIndex === 2}
                  onClick={() => check(2)}
                />
                <label htmlFor="s1" id="slide1">
                  {/* <img className="fea" src="./assets/img/img1.jpg" height="100%" width="100%"/> */}
                  <h1>Lorem Ipsum 01</h1>
                </label>
                <label htmlFor="s2" id="slide2">
                  {/* <img className="fea" src="./assets/img/img2.jpg" height="100%" width="100%"/> */}
                  <h1>Lorem Ipsum 02</h1>
                </label>
                <label htmlFor="s3" id="slide3">
                  {/* <img className="fea" src="./assets/img/img3.jpg" height="100%" width="100%"/> */}
                  <h1>Lorem Ipsum 03</h1>
                </label>
              </section>
              </Swipeable>
            </div>
            <div className="hidden lg:w-1/4 py-48 md:mb-0 mb-6 md:flex md:flex-col text-center justify-center items-start">
              <div className="w-20 h-20 hidden md:inline-flex items-center justify-center rounded-full lg:bg-blue-100 mb-5 flex-shrink-0">
                <button onClick={checkNext}>
                  <img
                    alt="logo"
                    className="next w-30 h-10 mb-1"
                    src="forward.svg"
                  ></img>
                </button>
              </div>
            </div>
          </div>
        </section>

        {/* feature section */}

        <section className="text-gray-700 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="text-center mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font text-gray-900 mb-4">
                OUR PRODUCT IS BEST SUITABLE FOR
              </h1>
              <div className="w-16 h-1 rounded-full bg-blue-500 inline-flex"></div>
            </div>
            <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4">
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-20 h-20 inline-flex items-center justify-center rounded-full bg-orange-100 text-orange-500 mb-5 flex-shrink-0">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    className="w-10 h-10"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                  </svg>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    Shooting Stars
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#1"
                    className="mt-3 text-orange-500 inline-flex items-center"
                  >
                    Learn More
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-4 h-4 ml-2"
                      viewBox="0 0 24 24"
                    >
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-20 h-20 inline-flex items-center justify-center rounded-full bg-orange-100 text-orange-500 mb-5 flex-shrink-0">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    className="w-10 h-10"
                    viewBox="0 0 24 24"
                  >
                    <path d="M22 12h-4l-3 9L9 3l-3 9H2"></path>
                  </svg>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    Shooting Stars
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#2"
                    className="mt-3 text-orange-500 inline-flex items-center"
                  >
                    Learn More
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-4 h-4 ml-2"
                      viewBox="0 0 24 24"
                    >
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-20 h-20 inline-flex items-center justify-center rounded-full bg-orange-100 text-orange-500 mb-5 flex-shrink-0">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    className="w-10 h-10"
                    viewBox="0 0 24 24"
                  >
                    <circle cx="6" cy="6" r="3"></circle>
                    <circle cx="6" cy="18" r="3"></circle>
                    <path d="M20 4L8.12 15.88M14.47 14.48L20 20M8.12 8.12L12 12"></path>
                  </svg>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    The Catalyzer
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#3"
                    className="mt-3 text-orange-500 inline-flex items-center"
                  >
                    Learn More
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-4 h-4 ml-2"
                      viewBox="0 0 24 24"
                    >
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
              <div className="p-4 md:w-1/4 md:mb-0 mb-6 flex flex-col text-center items-center">
                <div className="w-20 h-20 inline-flex items-center justify-center rounded-full bg-orange-100 text-orange-500 mb-5 flex-shrink-0">
                  <svg
                    fill="none"
                    stroke="currentColor"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    stroke-width="2"
                    className="w-10 h-10"
                    viewBox="0 0 24 24"
                  >
                    <path d="M20 21v-2a4 4 0 00-4-4H8a4 4 0 00-4 4v2"></path>
                    <circle cx="12" cy="7" r="4"></circle>
                  </svg>
                </div>
                <div className="flex-grow">
                  <h2 className="text-gray-900 text-lg title-font font-medium mb-3">
                    Neptune
                  </h2>
                  <p className="leading-relaxed text-base">
                    Blue bottle crucifix vinyl post-ironic four dollar toast
                    vegan taxidermy. Gastropub indxgo juice poutine, ramps
                    microdosing banh mi pug VHS try-hard.
                  </p>
                  <a
                    href="#4"
                    className="mt-3 text-orange-500 inline-flex items-center"
                  >
                    Learn More
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-4 h-4 ml-2"
                      viewBox="0 0 24 24"
                    >
                      <path d="M5 12h14M12 5l7 7-7 7"></path>
                    </svg>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* sample */}

        <section className="text-gray-700 body-font">
          <div className="container px-5 py-24 mx-auto">
            <div className="flex flex-col text-center w-full mb-20">
              <h1 className="sm:text-3xl text-2xl font-medium title-font mb-4 text-gray-900">
                NOISES WE CAN CANCEL
              </h1>
              <div className="w-16 h-1 m-auto rounded-full text-center bg-blue-500 inline-flex"></div>

              {/* <p className="lg:w-2/3 mx-auto leading-relaxed text-base">Whatever cardigan tote bag tumblr hexagon brooklyn asymmetrical gentrify, subway tile poke farm-to-table. Franzen you probably haven't heard of them man bun deep jianbing selfies heirloom.</p> */}
            </div>
            <div className="flex flex-wrap -m-4">
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/600x360"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Shooting Stars
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/601x361"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      The Catalyzer
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/603x363"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      The 400 Blows
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/602x362"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Neptune
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/605x365"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Holden Caulfield
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/606x366"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Alper Kamu
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/602x362"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Neptune
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/605x365"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Holden Caulfield
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
              <div className="lg:w-1/3 sm:w-1/3 p-4">
                <div className="flex relative">
                  <img
                    alt="gallery"
                    className="absolute inset-0 w-full h-full object-cover object-center"
                    src="https://dummyimage.com/606x366"
                  />
                  <div className="px-8 py-10 relative z-10 w-full border-4 border-gray-200 bg-white opacity-0 hover:opacity-100">
                    <h2 className="tracking-widest text-sm title-font font-medium text-indigo-500 mb-1">
                      THE SUBTITLE
                    </h2>
                    <h1 className="title-font text-lg font-medium text-gray-900 mb-3">
                      Alper Kamu
                    </h1>
                    <p className="leading-relaxed">
                      Photo booth fam kinfolk cold-pressed sriracha leggings
                      jianbing microdosing tousled waistcoat.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        {/* testimonial */}

        <div className="flex flex-wrap sm:-m-4 -mx-4 -mb-10 -mt-4 justify-center">
          <div className="md:w-1/4 py-64 md:mb-0 mb-6 flex flex-col text-center items-end sm:z-10">
            <div className="w-20 h-20 inline-flex items-center justify-center rounded-full md:bg-blue-100 text-blue-500 mb-5 flex-shrink-0">
              <button onClick={testimonialCheckPrev}>
                <img
                  alt="logo"
                  className="next w-30 h-10 mb-1"
                  src="back.svg"
                ></img>
              </button>
            </div>
          </div>
          <div className="md:w-2/4 md:mb-0 mb-6 flex flex-col text-center items-center">
            <Swipeable onSwipeLeft={testimonialCheckNext} onSwipeRight={testimonialCheckPrev}>
            <section id="testimonialSlider">
              <input
                type="radio"
                name="testimonialSlider"
                id="t1"
                checked={selectedTestimonialIndex === 0}
                onClick={() => testimonialCheck(0)}
              />
              <label htmlFor="t1" id="testimonialSlide1">
                <div className="w-64 sm:w-full lg:mb-0 p-4 mx-auto">
                  <div className="h-full -mt-20 text-center">
                    <img
                      alt="testimonial"
                      className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                      src={testimonialArray[i].src}
                    />
                    <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                      {testimonialArray[i].name}
                    </h2>
                    <p className="leading-relaxed text-gray-600">
                      {testimonialArray[i].text}
                    </p>
                  </div>
                </div>
              </label>

              <input
                type="radio"
                name="testimonialSlider"
                id="t2"
                checked={selectedTestimonialIndex === 1}
                onClick={() => testimonialCheck(1)}
              />
              <label htmlFor="t2" id="testimonialSlide2">
                <div className="w-64 sm:w-full lg:mb-0 p-4 mx-auto">
                  <div className="h-full -mt-20 text-center">
                    <img
                      alt="testimonial"
                      className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                      src={testimonialArray[i].src}
                    />
                    <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                      {testimonialArray[i].name}
                    </h2>
                    <p className=" sm:leading-none text-gray-600">
                      {testimonialArray[i].text}
                    </p>
                  </div>
                </div>
              </label>

              <input
                type="radio"
                name="testimonialSlider"
                id="t3"
                checked={selectedTestimonialIndex === 2}
                onClick={() => testimonialCheck(2)}
              />
              <label htmlFor="t3" id="testimonialSlide3">
                <div className="w-64 sm:w-full lg:mb-0 p-4 mx-auto">
                  <div className="h-full -mt-20 text-center">
                    <img
                      alt="testimonial"
                      className="w-16 h-16 mb-4 mt-6 sm:w-32 sm:h-32 sm:mb-8 object-cover object-center rounded-full inline-block border-2 border-gray-200 bg-gray-100"
                      src={testimonialArray[i].src}
                    />
                    <h2 className="text-gray-900 font-bold title-font tracking-wider text-xl mt-0 mb-10">
                      {testimonialArray[i].name}
                    </h2>
                    <p className="leading-relaxed text-gray-600">
                      {testimonialArray[i].text}
                    </p>
                  </div>
                </div>
              </label>
            </section>
            </Swipeable>
          </div>
          <div className="md:w-1/4 py-64 md:mb-0 mb-6 flex flex-col text-center items-start sm:z-10">
            <div className="w-20 h-20 inline-flex items-center justify-center rounded-full md:bg-blue-100 text-blue-500 mb-5 flex-shrink-0">
              <button onClick={testimonialCheckNext}>
                <img
                  alt="logo"
                  className="next w-30 h-10 mb-1"
                  src="forward.svg"
                ></img>
              </button>
            </div>
          </div>
        </div>

        {/* footer */}

        <footer className="text-gray-700 body-font">
          <div className="container px-1 py-8  mb-0 mx-auto flex items-center sm:flex-row flex-col">
            <img alt="logo" className="w-40 h-8 mb-8" src="logo.png"></img>
            {/* <span className="ml-3 mb-6 text-xl sm:ml-4 sm:pl-4 sm:border-l-2 sm:border-gray-200">
              SpeechCorp.
            </span> */}
          </div>
          <div className="container px-5 py-12 pt-0 mx-auto">
            <div className="flex flex-wrap md:text-left text-center -mb-10 -mx-4">
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                  APP
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">Product</a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">Pricing</a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Download
                    </a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                  ABOUT US
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">Team</a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">Career</a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Sponsors
                    </a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">Contact</a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                  SUPPORT
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Email Us
                    </a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Report a bug
                    </a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">FAQ</a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 md:w-1/2 text-left w-full px-4">
                <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                  LEGAL
                </h2>
                <nav className="list-none mb-10">
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Privacy policies
                    </a>
                  </li>
                  <li>
                    <a className="text-gray-600 hover:text-gray-800">
                      Terms and conditions
                    </a>
                  </li>
                </nav>
              </div>
              <div className="lg:w-1/5 text-left w-full px-4">
                <h2 className="title-font font-medium text-gray-900 tracking-widest text-sm mb-3">
                  SOCIAL
                </h2>
                <span className="inline-flex lg:ml-auto lg:mt-0 mt-6 w-full justify-center md:justify-start md:w-auto">
                  <a className="text-gray-500">
                    <svg
                      fill="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M18 2h-3a5 5 0 00-5 5v3H7v4h3v8h4v-8h3l1-4h-4V7a1 1 0 011-1h3z"></path>
                    </svg>
                  </a>
                  <a className="ml-3 text-gray-500">
                    <svg
                      fill="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path d="M23 3a10.9 10.9 0 01-3.14 1.53 4.48 4.48 0 00-7.86 3v1A10.66 10.66 0 013 4s-4 9 5 13a11.64 11.64 0 01-7 2c9 5 20 0 20-11.5a4.5 4.5 0 00-.08-.83A7.72 7.72 0 0023 3z"></path>
                    </svg>
                  </a>
                  <a className="ml-3 text-gray-500">
                    <svg
                      fill="none"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="2"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <rect
                        width="20"
                        height="20"
                        x="2"
                        y="2"
                        rx="5"
                        ry="5"
                      ></rect>
                      <path d="M16 11.37A4 4 0 1112.63 8 4 4 0 0116 11.37zm1.5-4.87h.01"></path>
                    </svg>
                  </a>
                  <a className="ml-3 text-gray-500">
                    <svg
                      fill="currentColor"
                      stroke="currentColor"
                      stroke-linecap="round"
                      stroke-linejoin="round"
                      stroke-width="0"
                      className="w-5 h-5"
                      viewBox="0 0 24 24"
                    >
                      <path
                        stroke="none"
                        d="M16 8a6 6 0 016 6v7h-4v-7a2 2 0 00-2-2 2 2 0 00-2 2v7h-4v-7a6 6 0 016-6zM2 9h4v12H2z"
                      ></path>
                      <circle cx="4" cy="4" r="2" stroke="none"></circle>
                    </svg>
                  </a>
                </span>
              </div>
            </div>
          </div>
          <div className="bg-gray-200">
            <div className="container mx-auto py-4 px-5 flex flex-wrap flex-col sm:flex-row">
              <p className="text-gray-500 text-sm text-center sm:text-left">
                © 2020 Speech Corp —
                <a
                  href="https://twitter.com/knyttneve"
                  className="text-gray-600 ml-1"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  All Rights Reserved.
                </a>
              </p>
              {/* <span className="sm:ml-auto sm:mt-0 mt-2 sm:w-auto w-full sm:text-left text-center text-gray-500 text-sm">
                Address -
              </span> */}
            </div>
          </div>
        </footer>
      </main>
    </div>
  );
}
